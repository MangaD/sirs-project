# Snort

**Tutorial:** https://www.hackingarticles.in/configure-snort-in-ubuntu-easy-way/

Snort is software created by Martin Roesch, which is widely use as Intrusion Prevention System (IPS) and Intrusion Detection System (IDS) in network. It is separated into the **five** most important mechanisms for instance:

- Detection engine
- Logging and alerting system
- Packet decoder
- Preprocessor
- Output modules

The program is quite famous to carry out real-time traffic analysis, also used to detect query or attacks, packet logging on Internet Protocol networks, to detect malicious activity, denial of service attacks and port scans by monitoring network traffic, buffer overflows, server message block probes, and stealth port scans.

Snort can be configured in **three** main modes:

- Sniffer mode: it will observe network packets and present them on the console.

- Packet logger mode: it will record packets to the disk.

- Intrusion detection mode: the program will monitor network traffic and analyze it against a rule set defined by the user.

After that the application will execute a precise action depending upon what has been identified.


### Snort Installation

We had chosen ubuntu 18.04 operating system for installation and configuration of snort. Earlier than installing snort in your machine, you should need to install necessary dependencies of ubuntu.

```sh
sudo apt install net-tools
```

Check your network interface configuration by executing `ifconfig` command; from here I came to know `192.168.1.1` is my local IP to outside and `192.168.2.100` is my local ip to private network, 

```sh
$ ifconfig
enp0s3: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.1.1  netmask 255.255.255.0  broadcast 192.168.1.255
        inet6 fe80::a00:27ff:fea7:14e6  prefixlen 64  scopeid 0x20<link>
        ether 08:00:27:a7:14:e6  txqueuelen 1000  (Ethernet)
        RX packets 4571  bytes 342547 (342.5 KB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 1674  bytes 421436 (421.4 KB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

enp0s8: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.2.100  netmask 255.255.255.0  broadcast 192.168.2.255
        inet6 fe80::a00:27ff:fe2c:8261  prefixlen 64  scopeid 0x20<link>
        ether 08:00:27:2c:82:61  txqueuelen 1000  (Ethernet)
        RX packets 2072  bytes 383877 (383.8 KB)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 2121  bytes 201288 (201.2 KB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0
```

```sh
sudo apt-get install snort*
```

Interface(s) which Snort should listen on:

`enp0s8` in my case.

Address range for the local network:

My netmask is `255.255.255.0` and inet is `192.168.2.100`.

So: 192.168.2.0/24

Open the configuration file using nano to make some changes.

```sh
sudo nano /etc/snort/snort.conf
```

Change `ipvar HOME_NET any` to `ipvar HOME_NET 192.168.2.0/24`

Now run the command below to enable the IDS mode of snort:

```sh
sudo snort -A console -i enp0s8 -c /etc/snort/snort.conf
```

Now it will compile the complete file and test the configuration setting automatically.


### Run snort and generate ascii output

```sh
sudo su
snort -A console -q -c /etc/snort/snort.conf -i enp0s8 -l /var/log/snort/ -K ascii
```