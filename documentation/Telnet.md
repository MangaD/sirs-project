# Install Telnet server

On the VM’s to be infected (VM2 and VM3), we’ll go to:

`Settings -> Network -> Adapter X`

And set: 
```
Attached to: Bridged adapter
Name: wlp3s0
```
So that we get network access for installing necessary software.

Tutorial: [https://askubuntu.com/questions/439574/telnet-server-not-starting](https://askubuntu.com/questions/439574/telnet-server-not-starting)
```sh
sudo apt-get install xinetd telnetd
```
Create file `telnet` and put in `/etc/xinetd.d`

`sudo nano /etc/xinetd.d/telnet`

```
# default: on
# description: The telnet server serves telnet sessions; it uses
# unencrypted username/password pairs for authentication.
service telnet
{
disable = no
flags = REUSE
socket_type = stream
wait = no
user = root
server = /usr/sbin/in.telnetd
log_on_failure += USERID
}
```

Restart xinetd service

`sudo service xinetd restart`





