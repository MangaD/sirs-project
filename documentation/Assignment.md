# DDoS Attack Mitigation

DDoS attack that take down servers may be launched via a botnet that enslaves a large number of devices exposed to the Internet and weakly protected devices, like IoT sensors. Attacks can be detected/prevented at the source (client), the destination (server), or in transit (edge). The goal of this project is to design and implement mitigation mechanism for a DDoS attack on a small network. The project consists of three parts:

- Create a (small) botnet. Create a client program to run on infected devices with a self-propagation mechanism that scans public IP addresses for insecure devices and try to access them over the telnet protocol using default login credentials. Lightweight client programs must be implemented to perform the attack (e.g. ping/UDP flooding) from the infected devices. To simulate the DDoS attack you may resort to other existing tools.

- Set-up an Intrusion Detection System at the server. The IDS must be integrated with firewall and router to enable rate-limiting and QoS provisioning. You can use systems
such as Snort* or Zeek (formerly Bro†)

- Propose solutions that exploit limited resources on edge devices to detect/prevent attacks in transit.