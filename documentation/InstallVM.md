# Install Virtual Machines

## Create a virtual machine instance

Each virtual machine instance will have its own virtual disk.
The virtual disk image can be obtained at:  
[http://disciplinas.tecnico.ulisboa.pt/~SIRS.daemon/2018-2019/sirs-vm-1819-v3.ova](http://disciplinas.tecnico.ulisboa.pt/~SIRS.daemon/2018-2019/sirs-vm-1819-v3.ova)

**Accounts:**

Username: fireman  
Password: 1ns3cur3

Username: root  
Password: 1ns3cur3

**Note:**

1. If the image appears distorted, try the following commands in order:

    - Ctrl+Alt F1 (or Host+F1)
    - Ctr+Alt F7 (or Host+F7)

2. You may wish to install Virtual Box Guest additions for a better experience. We won't cover that here.

3. For persistant `iptables` we may install the following:
```sh
$ sudo apt install iptables-persistent
```

And execute the following after setting the `iptables`:

```sh
iptables-save > /etc/iptables/rules.v4
```


Create a virtual machine named **HackerVM** by executing the following steps:

1. Open Virtual Box
2. Create a Virtual Machine:
    - File -> Import Appliance...
    - Choose the **sirs-vm-1819-v3.ova** file
    - Click "Next"
    - Change the name to "HackerVM"
    - If space is a problem, change the path of Virtual Disk Image to another partition.
    - Pick: "Reinitialize the MAC address of all network cards"
    - Click: "Import"
3. Repeat step 2 to create "Vuln1", "Vuln2", ..., "UserVM", "GatewayVM", "FakeCloudVM", "SnortVM", "ServerVM"


## Create the virtual network

We will now configure the network settings of each Guest.
**GatewayVM** will access both the physical network and the virtual network.

To configure the access to the physical network, go to Network Adapters and Other Hardware configuration:

1. VirtualBox menu: select **GatewayVM -> Settings... -> Network**
2. Network menu: select **Adapter 1**. Click on "Enable Network Adapter" to enable the adapter, if the checkbox is not already checked. **Attached to: "Bridged Adapter"**.
3. Name: Select the default network card: on Windows ("Intel(R) Pro...", "RealTek...", "Wifi","Ethernet", etc); on Linux (eth0, wlan0, etc); on MacOS (en0: Wi-Fi (AirPort)) , so that the virtual machine virtual is added to the host virtual network.
4. Expand **Advanced**
    1. Adapter Type: Intel PRO/1000 MT Desktop (82540EM)
    2. Promiscuous Mode: Allow All
    3. MAC Address: use default value
    4. Verify that "cable connected" is enabled.

***Note:** HackerVM, Vulnerable VM's (Vuln1, Vuln2...) and UserVM will be connected to the same physical network, so you must follow steps 1-4 above for them as well.*

To configure the virtual network:

1. VirtualBox menu: select **GatewayVM -> Settings... -> Network**
2. Network menu: select **Adapter 2**. Click on "Enable Network Adapter" to enable the adapter, if
the checkbox is not already checked. **Attached to: "Internal Network".**
3. Name: Select the default network card: on Windows ("Intel(R) Pro...", "RealTek...",
"Wifi","Ethernet", etc); on Linux (eth0, wlan0, etc); on MacOS (en0: Wi-Fi (AirPort)), so that the virtual machine virtual is added to the host virtual network.  
***Note:** if the only option is 'intnet", choose that option.*
4. Expand **Advanced**
    1. Adapter Type: Intel PRO/1000 MT Desktop (82540EM)
    2. Promiscuous Mode: Allow All
    3. MAC Address: use default value
    4. Verify that "cable connected" is enabled.

***Note:** FakeCloudVM, SnortVM, ServerVM will be connected to the same virtual network, so you must follow steps 1-4 above for them, but use Adapter 1. SnortVM will have 2 adapters for internal network.*


## Configure the virtual network

The physical and virtual networks will be different IP subnets. For the virtual network, we will use the private IP addresses 192.168.1.0/24 (meaning that the subnet
mask is 255.255.255.0 – we can have 254 addresses to use - from 192.168.1.1 to 192.168.1.254 – 192.168.1.255 is reserved for broadcast). For the physical network we will use the IP addresses 192.168.0.0/24.

The network layout will be as follows:

HackerVM: 192.168.0.12  
Vuln1: 192.168.0.14  
Vuln2: 192.168.0.17  
Vuln3: 192.168.0.X ...  
UserVM: 192.168.0.20  
GatewayVM: 192.168.0.10, 192.168.1.254  
SnortVM: 192.168.1.1, 192.168.2.100  
FakeCloudVM: 192.168.3.1  
ServerVM: 192.168.2.1

Configuration for **HackerVM**:

Note: since a lot the network files are located at `/etc/netplan` directory, it is advised to navigate to it to make command execution easier:
```sh
$ cd /etc/netplan
```

Using a basic editor – like vi, nano or leafpad - set the IP and Netmask values (you may use the following example contents)
```sh
$ sudo nano /etc/netplan/01-netcfg.yaml
```
Write the following content:

***Attention:** Do not use tabs to indent code, use 2 spaces instead.*

```yaml
network:
  version: 2
  renderer: networkd
  ethernets:
    enp0s3:
      dhcp4: no
      dhcp6: no
      addresses: [192.168.0.12/24]
      gateway4: 192.168.0.10
      routes:
      - to: 0.0.0.0/0
        via: 192.168.0.1
```

When you finish writing, press `ctrl+o` to write and `ctrl+x` to exit the editor.

To apply the chages execute:
```sh
$ sudo netplan apply
```
Verify that the machine has the correct IP address by executing:

```sh
$ hostname -I
```

If the IP is not right, check the network icon at the panel and make sure `netplan-enp0s3` is selected instead of `Wired connection 1`

Apply the same procedure to configure the vulnerable VM's with IP 192.168.0.14, 192.168.0.17..., and UserVM with IP 192.168.0.20 as indicated in the layout above. You can create as many vulnerable machines as you like and give any number to the last number of the IP, so long it is in the range 192.168.0.2 - 192.168.0.254 and not already in use by another VM.

Configuration for **ServerVM**:

```yaml
network:
  version: 2
  renderer: networkd
  ethernets:
    enp0s3:
      dhcp4: no
      dhcp6: no
      addresses: [192.168.2.1/24]
      gateway4: 192.168.2.100
```

Configure `iptables`:

```sh
#flush iptables chains
iptables -F
iptables -t nat -F
iptables -X

#only accept connections from the snort in IPS mode
#only reply to snort machine too (outgoing)
iptables -A INPUT -s 192.168.1.1 -j ACCEPT
iptables -A OUTPUT -d 192.168.1.1 -j ACCEPT
iptables -A INPUT -j DROP
iptables -A OUTPUT -j DROP

#save the chains persistentily (needs packet iptables-persistent)
iptables-save > /etc/iptables/rules.v4
```

Apply the same procedure to **FakeCloudVM** with IP 192.168.3.1

Configuration for **SnortVM**:

```yaml
network:
  version: 2
  renderer: networkd
  ethernets:
    enp0s3:
      dhcp4: no
      dhcp6: no
      addresses: [192.168.1.1/24]
      gateway4: 192.168.1.254
      routes:
      - to: 192.168.0.0/24
        via: 192.168.1.254
      match:
        macaddress: XX:XX:XX:XX:XX:XX
    enp0s8:
      dhcp4: no
      dhcp6: no
      addresses: [192.168.2.100/24]
      gateway4: 192.168.2.1
      match:
        macaddress: YY:YY:YY:YY:YY:YY
```

Note that you have to find the correct MAC address of each network interface given by Virtual Box. You can consult those values by selecting **SnortVM -> Settings -> Network**.

Configure `iptables`:

```sh
#flush iptables chains
iptables -F
iptables -t nat -F
iptables -X

#reroute all incoming connections to webserver
iptables -t nat -A PREROUTING -j DNAT --to-destination 192.168.2.1
iptables -t nat -A POSTROUTING -p tcp -d 192.168.2.1 -j SNAT --to-source 192.168.1.1

#only receive connections from the firewall
#only reply to firewall machine too (outgoing)
iptables -A INPUT -s 192.168.0.10 -j ACCEPT
iptables -A OUTPUT -d 192.168.0.10 -j ACCEPT
iptables -A INPUT -j DROP
iptables -A OUTPUT -j DROP

#save the chains persistentily (needs packet iptables-persistent)
iptables-save > /etc/iptables/rules.v4
```

Since SnortVM will be the default gateway for ServerVM and FakeCloudVM, IP forwarding must be enabled. This will allow them to communicate with machines outside their subnet.

In SnortVM open the file `/etc/sysctl.conf` and change/add the following line to:

`net.ipv4.ip_forward = 1`

Restart SnortVM.

Confirm you will get 1 by executing the following command:

`/sbin/sysctl net.ipv4.ip_forward`

If you get 0 the execute the following:

`/sbin/sysctl -w net.ipv4.ip_forward=1`


Configuration for **GatewayVM**:

```yaml
network:
  version: 2
  renderer: networkd
  ethernets:
    enp0s3:
      dhcp4: no
      dhcp6: no
      addresses: [192.168.0.10/24]
      gateway4: 192.168.0.1
      match:
        macaddress: XX:XX:XX:XX:XX:XX
    enp0s8:
      dhcp4: no
      dhcp6: no
      addresses: [192.168.1.254/24]
      gateway4: 192.168.1.1
      match:
        macaddress: YY:YY:YY:YY:YY:YY
```

Note that you have to find the correct MAC address of each network interface given by Virtual Box. You can consult those values by selecting **GatewayVM -> Settings -> Network**.

To apply the changes execute:
```sh
$ sudo netplan apply
```
Verify that the machine has the correct IP address by executing:
```sh
$ hostname -I
```

Configure `iptables`:

```sh
#flush and load default policies
iptables -P FORWARD ACCEPT
iptables -P INPUT DROP
iptables -P OUTPUT ACCEPT
iptables -F
iptables -t nat -F
iptables -t mangle -F
iptables -X

#nat prerouting 
iptables -t nat -A PREROUTING -i enp0s3 -p tcp --dport 80  -j DNAT --to 192.168.1.1:80
iptables -t nat -A PREROUTING -i enp0s3 -p tcp --dport 443  -j DNAT --to 192.168.1.1:443
iptables -t nat -A PREROUTING -i enp0s3 -p tcp --dport 23  -j DNAT --to 192.168.1.1:23

#postrouting
iptables -t nat -A POSTROUTING -p all -s 192.168.1.1 -o enp0s3 -j MASQUERADE

#some anti-ddos mitigation through mangle chain

#icmp packets (usually pings)
iptables -t mangle -A PREROUTING -p icmp -j DROP

#drop invalid packets
iptables -t mangle -A PREROUTING -m conntrack --ctstate INVALID -j DROP  

#drop TCP packets that are new and are not SYN
iptables -t mangle -A PREROUTING -p tcp ! --syn -m conntrack --ctstate NEW -j DROP 
 
#drop SYN packets with suspicious MSS value
iptables -t mangle -A PREROUTING -p tcp -m conntrack --ctstate NEW -m tcpmss ! --mss 536:65535 -j DROP  

#block packets with bogus TCP flags
iptables -t mangle -A PREROUTING -p tcp --tcp-flags FIN,SYN,RST,PSH,ACK,URG NONE -j DROP 
iptables -t mangle -A PREROUTING -p tcp --tcp-flags FIN,SYN FIN,SYN -j DROP 
iptables -t mangle -A PREROUTING -p tcp --tcp-flags SYN,RST SYN,RST -j DROP 
iptables -t mangle -A PREROUTING -p tcp --tcp-flags FIN,RST FIN,RST -j DROP 
iptables -t mangle -A PREROUTING -p tcp --tcp-flags FIN,ACK FIN -j DROP 
iptables -t mangle -A PREROUTING -p tcp --tcp-flags ACK,URG URG -j DROP 
iptables -t mangle -A PREROUTING -p tcp --tcp-flags ACK,FIN FIN -j DROP 
iptables -t mangle -A PREROUTING -p tcp --tcp-flags ACK,PSH PSH -j DROP 
iptables -t mangle -A PREROUTING -p tcp --tcp-flags ALL ALL -j DROP 
iptables -t mangle -A PREROUTING -p tcp --tcp-flags ALL NONE -j DROP 
iptables -t mangle -A PREROUTING -p tcp --tcp-flags ALL FIN,PSH,URG -j DROP 
iptables -t mangle -A PREROUTING -p tcp --tcp-flags ALL SYN,FIN,PSH,URG -j DROP 
iptables -t mangle -A PREROUTING -p tcp --tcp-flags ALL SYN,RST,ACK,FIN,URG -j DROP  

#block (probable) spoofed packets (private aka local subnets) 
iptables -t mangle -A PREROUTING -s 224.0.0.0/3 -j DROP 
iptables -t mangle -A PREROUTING -s 169.254.0.0/16 -j DROP 
iptables -t mangle -A PREROUTING -s 172.16.0.0/12 -j DROP 
iptables -t mangle -A PREROUTING -s 192.0.2.0/24 -j DROP 
#since we gonna test through this ip, just let it comment so isn't blocked
#iptables -t mangle -A PREROUTING -s 192.168.0.0/16 -j DROP 
iptables -t mangle -A PREROUTING -s 10.0.0.0/8 -j DROP 
iptables -t mangle -A PREROUTING -s 0.0.0.0/8 -j DROP 
iptables -t mangle -A PREROUTING -s 240.0.0.0/5 -j DROP 
iptables -t mangle -A PREROUTING -s 127.0.0.0/8 ! -i lo -j DROP

#save the chains persistentily (needs packet iptables-persistent)
iptables-save > /etc/iptables/rules.v4
```

GatewayVM should now be able to ping other VM's (and vice-versa):
```sh
$ ping 192.168.0.12
```

Since GatewayVM will be the default gateway for SnortVM, IP forwarding must be enabled. This will allow SnortVM to communicate with machines outside the subnet 192.168.1.X.

In GatewayVM open the file `/etc/sysctl.conf` and change/add the following line to:

`net.ipv4.ip_forward = 1`

Restart GatewayVM.

Confirm you will get 1 by executing the following command:

`/sbin/sysctl net.ipv4.ip_forward`

If you get 0 the execute the following:

`/sbin/sysctl -w net.ipv4.ip_forward=1`
