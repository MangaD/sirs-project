# DDoS Attack Mitigation

**Course:** Network and Computer Security  
**University:** Instituto Superior Técnico  
**Academic year:** 2018-19

## TO-DO

- Explicacao.docx to English and markdown
- Explain that `folder_checker.py` should have been replaced with Snort triggers
- Explain that QoS wasn't implemented
- Explain that more Snort rules should be added (eg. SYN flood)
- More things on "Testing" section at the end of this file

### Team

- David Gonçalves
- Luís Rodrigues


## Assignment

See `documentation/Assignment.md`

## Plan

We use a virtual network consisting of multiple virtual machines supported by [Oracle's VirtualBox](https://www.virtualbox.org/) virtualization software. VirtualBox works with Windows, Mac and Linux operating systems, although specific instructions may vary slightly. The physical machine is called the Host whereas the virtual machines are called Guests. The host will be the computer running VirtualBox.

We'll set a few virtual machines for testing a DDoS attack against a local HTTP server and DDoS mitigation using firewall rules and [Snort](https://www.snort.org/).

Using the virtual disk from the course lab is a good idea because it is lightweight and already has telnet installed. Although, any lightweight OS can be used as long as `netplan`, `telnetd` and `xinetd` are installed.

**Virtual disk download:** [http://disciplinas.tecnico.ulisboa.pt/~SIRS.daemon/2018-2019/sirs-vm-1819-v3.ova](http://disciplinas.tecnico.ulisboa.pt/~SIRS.daemon/2018-2019/sirs-vm-1819-v3.ova)

Each guest will use a version of CentOS Linux with low memory requirements.

### Network layout

**Hacker VM:** This VM will simulate the machine of the attacker. It will be on the physical network and launch a script to find vulnerable machines on the network (using telnet and default credentials) and inject code in them to DoS our target (flood of HTTP requests). In theory we'll have multiple machines infected, and thus a DDoS.

In the infected machines we have 2 options for our malicious script:

1. Install [jMeter](https://jmeter.apache.org/) and configure it to DoS the target.
2. Create our own script or use an available script on the internet to DoS the target.

We chose to use a python script we found on the internet as it would be faster than installing jMeter.

**Vulnerable VM's:** We can have as many of these as our physical resources allow. These will be on the physical network.

**User VM:** A normal user that will attempt to view the website hosted in ServerVM. This will be on the physical network.

**Gateway VM:** This VM will access both the physical network and the virtual network, it will operate as a gateway between the two subnets.

**Snort VM:** This VM will operate as a gateway between GatewayVM and the other VM's in the virtual network. This will be on the virtual network. The IDS (Snort) can be installed on a VM that filters the requests (our case) or in the target VM (more simple and direct). We must create rules in Snort that detect the attacks (number of requests per second) and do something about it (eg. temporarily block the offender IP's and send an e-mail to the admin.)

**FakeCloudVM:** To simulate a cloud service and, when under attack, reroute the traffic coming from snort to it. This will be on the virtual network.

**Server VM:** This VM will host apache web server and a sample webpage that will be the target of our DDoS. It will be in the virtual network.


### Installing the VM's

See `documentation/InstallVM.md`


### Setting up VM's

**HackerVM:** Place `telnet-infect.py` and `doser.py` in the desktop (found in folder `configurations/HackerVM/`). We will be running the script `telnet-infect.py` on this machine, which will scan all IP addresses on the physical subnet (192.168.0.1 - 192.168.0.254) in search for a listening telnet server with default credentials, and will place and run `doser.py` on these vulnerable machines to DDoS the target HTTP server running on ServerVM.

**Vuln VM's:** If using the recommended virtual disk everything should be set. If not, taking a look at `documentation/Telnet.md` may help.

**User VM:** Disable telnet with:

```sh
$ sudo systemctl stop xinetd
```

**SnortVM:** Place the following files (found in folder `configurations/SnortVM/`) in some folder inside this VM:

- `folder_checker.py`
- `netplan_normal.yaml`
- `netplan_redirect.yaml`
- `policies`
- `policies_redirect`

Install and run Snort: see `documentation/Snort.md`

Place `snort.conf` in `/etc/snort/snort.conf`.
Place `snort_rules` in `/etc/snort/rules/local.rules`.

Run:

```sh
$ sudo python folder_checker.py
```

**GatewayVM:** Disable telnet with:

```sh
$ sudo systemctl stop xinetd
```

**ServerVM:** We want apache server running, but our VM's have OpenVAS installed and listening on port 80. We will stop it and start apache.

```sh
$ sudo pkill gsad
$ sudo systemctl start apache2
```

**FakeCloudVM:** To distinguish from the actual server do the following:

```sh
$ sudo pkill gsad
$ sudo systemctl start apache2
$ sudo bash -c 'echo "<h1> REROUTED </h1>" > /var/www/html/index.html'
```


## Testing

Run `telnet-infect.py` on HackerVM.
