import time
from os import system
import smtplib
from checksumdir import dirhash

def GetHashofDir(directory, verbose=0):
	return dirhash(directory, 'md5')	 

if __name__ == "__main__":
	directory = "/var/log/snort/"
	last_hash = GetHashofDir(directory,0);
	changed = False;
	changed_iterations = 0;
	gmail_user = "ddosmitigation123@gmail.com"
	gmail_password = "sirsTEST2018"
	source_email = "ddosmitigation123@gmail.com"
	to_email = "luisrodrigues154.lr@gmail.com"
	subject = "Traffic redirection"
	body = "The traffic was redirected to cloud service"
	email_text = "From: {0}\nTo: {1}\nSubject: {2}\n{3}".format(source_email, to_email, subject, body)
	
	#server = smtplib.SMTP('smtp.gmail.com: 587')
	#server.starttls()
	#server.login(gmail_user, gmail_password )
	while True:
		hash_of_log = GetHashofDir(directory,0)
		if(last_hash == hash_of_log):
			print("\tNo hash modifications...")
			if(changed == True):
				print("\tON CLOUD route\n")			
				changed_iterations+=1
			else:
				print("\tON NORMAL route\n")
			if(changed_iterations == 10):
				print("\t----------------------------------------\n\t\t50 seconds no alert...\n\t\t-> REVERTING TO NORMAL <-")
				print ("\t\tSame hash {0} times\n\t----------------------------------------\n".format(changed_iterations))
				#50 senconds without changes route to server normally
				changed_iterations = 0
				changed = False;
				system('cat netplan_normal.yaml  > /etc/netplan/01-netcfg.yaml')
				system('netplan apply')
				system('./policies')
			time.sleep(5)
		else:
			last_hash = hash_of_log
			if(changed == False):
				changed = True;
				print("\t----------------------------------------\n\t\tDIFFERENT HASH DETECTED\n\t\t-> REROUTING <-\n\t----------------------------------------\n")
				#message = "\nSnort machine redirect traffic to cloud"
				#execute the reroute files and
				system('cat netplan_redirect.yaml  > /etc/netplan/01-netcfg.yaml')
				system('netplan apply')
				system('./policies_redirect')
				#server.sendmail(source_email,to_email, email_text)
				time.sleep(5)
			else:
				print("\t----------------------------------------\n\t\tANOTHER HASH DETECTED\n\t\t-> STILL REROUTED <- \n\t----------------------------------------\n")
				changed_iterations = 0